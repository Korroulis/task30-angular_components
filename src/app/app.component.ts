import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-task30';

  // Switch between register & login form when button is clicked
  buttonWasClicked: boolean = false;

  setButtonClicked(clicked: boolean) {
      this.buttonWasClicked = !this.buttonWasClicked;
  }

  loginAttempted(message: string): void {
    alert(message);
  }

  signupAttempted(message: string): void {
    alert(message);
  }

}




