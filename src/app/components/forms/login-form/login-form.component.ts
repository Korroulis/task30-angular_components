import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent implements OnInit {

  //@Input() loginTitle: string;

  @Output () loginEvent: EventEmitter<string> = new EventEmitter();

  public loginTitle: string = 'Login to the super duper app';
  //private Result: any

  constructor() { }

  ngOnInit(): void {

  }

  loginClick() {

    //const result

    this.loginEvent.emit('Login attempted')
  }

} 

