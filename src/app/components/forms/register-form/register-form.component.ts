import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  @Output () signupEvent: EventEmitter<string> = new EventEmitter();

  public signupTitle: string = 'Register to the super duper app';

  constructor() { }

  ngOnInit(): void {
  }

  SignupClick() {

    //const result
    this.signupEvent.emit('Signup attempted')

    
  }
}
